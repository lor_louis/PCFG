#!/bin/bash
# this scipt installs the dependancies and copies the files to make i3 work correctly
local dir
printf "This script will install/update i3-wm, i3Blocks, i3lock, wireless_tools, imagemagick, feh, scrot, xbacklight, gksu, pm-utils powerline, powerline-fonts, xcompmgr and rxvt-unicode.\n" 
read -n 1 -p "Are you sure you want to proceed?: " uIn

case ${uIN:0:1} in
    n|N ) exit;;
esac
printf "\n"
#acpi is not needed on desktop
    read -n 1 -p "Do you wish to install acpi to display you battery percentage in i3Blocks?: " uIn
case ${uIn:0:1} in 
    y|Y ) sudo pacman -S --noconfirm --needed acpi;;
esac
printf "\n"
sudo pacman -S --noconfirm --needed i3-wm i3Blocks i3lock wireless_tools imagemagick feh scrot xbacklight gksu pm-utils xcompmgr rxvt-unicode python3 vim powerline powerline-fonts
dir=$(pwd)
sudo cp -r $dir/etc/i3* /etc/
sudo cp -r i3 ~.config/
sudo cp Pictures ~
cp Xdefault ~/.Xdefault

