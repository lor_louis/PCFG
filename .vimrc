execute pathogen#infect()
syntax enable
set background=dark
colorscheme solarized
hi Normal ctermbg=none
set nu

python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
    
set laststatus=2
